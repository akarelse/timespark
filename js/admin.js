$(document).ready(function() {
    $("#datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    timetimer = setInterval(updatevalues, 1000);

    $("[type|='submit']").hover(function() {
        clearInterval(timetimer);
    });


    function updatevalues() {
        $.get("admin/timespark", function(data, status) {
            $("#currentdate").html(data);
        });
    }
    $("div.buttons").append("<a class='btn green'>Internet time</a>");
    $("div.buttons a.green").click(function() {
        $.get("admin/timespark/setntptime", function(data, status) {
            setTimeout(removealert, 5000);
            dataar = data.split(',');
            $("#content-body").prepend("<div class='alert " + dataar[0] + "'><p>" + dataar[1] + "</p></div>")
            $("#content-body .alert").css('opacity', '0.0');
            $("#content-body .alert").fadeTo( "slow" , 1);
        });
    });

    function removealert() {
        $("#content-body .alert").fadeTo( "slow" , 0);
        pyro.clear_notifications();
    }



});