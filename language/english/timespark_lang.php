<?php
//messages
$lang['timespark:success']			=	'It worked';
$lang['timespark:error']			=	'It didn\'t work';


//page titles


//labels
$lang['timespark:name']				=	'Name';
$lang['timespark:slug']				=	'Slug';
$lang['timespark:manage']			=	'Manage';
$lang['timespark:item_list']		=	'Time Spark';
$lang['timespark:date']				=	'Date';
$lang['timespark:hours']			=	'Hours';
$lang['timespark:minutes']			=	'Minutes';
$lang['timespark:current']			=	'Current: ';


//buttons
$lang['timespark:items']			=	'Time Spark';
?>