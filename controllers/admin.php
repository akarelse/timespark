<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// use Bt52\NTP\Socket;
// use Bt52\NTP\Client;

// require('../libraries/Socket.php');

/**
 * This is a timespark module for PyroCMS
 *
 * @author
 * @website
 * @package
 * @subpackage
 */
class Admin extends Admin_Controller
{
    
    public function __construct() {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->library('socket');
        $this->load->library('client');
        $this->lang->load('timespark');
        
        $this->template->append_js('module::admin.js')->append_css('module::admin.css');
        
        $this->item_validation_rules = array(
            array(
                'field' => 'cdate',
                'label' => 'Name',
                'rules' => 'trim|required'
            ) ,
            array(
                'field' => 'hour',
                'label' => 'Slug',
                'rules' => 'trim|integer|required'
            ) ,
            array(
                'field' => 'minute',
                'label' => 'Max',
                'rules' => 'trim|integer|required'
            )
        );
    }

public function zoink()
{
        print($this->session->flashdata('success'));
}
    
    /**
     * List all items
     */
    public function index() {
        
        $d1 = new DateTime();
        
        $data = new stdClass();
        
        $data = $this->hours_minutes_seconds($data);
        
        $this->input->is_ajax_request() and $this->template->set_layout(FALSE);
        
        $this->template->title($this->module_details['name'])
        ->set('cdate', $d1->format("d-m-Y"))->set('chour', $d1->format("H"))
        ->set('currenttime', $d1->format("d-m-Y H:i:s"))
        ->set('cminute', $d1->format("i"));
        
        $this->input->is_ajax_request() ? $this->template->build('admin/ajaxitems', $data) : $this->template->build('admin/items', $data);
    }
    
    public function settime() {
        if ($input = $this->input->post()) {
            unset($input['btnAction']);
            $this->form_validation->set_rules($this->item_validation_rules);
            $this->form_validation->set_data($input);
            
            if ($this->form_validation->run()) {
                $os = $this->operating_system();
                $d1 = new DateTime($input['cdate'] . $input['hour'] . ":" . $input['minute'] . ":0");
                if ($os == 'linux') {
                    exec("sudo date -s '" . $d1->format("d M Y H:i:s") . "' 2>&1", $error);
                    if (isset($error[1])) {
                        $this->session->set_flashdata('error', $error[0]);
                        redirect("admin/timespark");
                    } else {
                        $this->session->set_flashdata('success', "time set !");
                        redirect("admin/timespark");
                    }
                } elseif ($os == 'win') {
                    exec("date -s '" . $d1->format("M/d/Y") . "' 2>&1", $error1);
                    exec("time -s '" . $d1->format("g:i:s A") . "' 2>&1", $error2);
                }
            }
        }
        
        
    }

    public function setntptime()
    {
        $socket = new Socket();
        $socket->setAll($this->settings->ntpserver, $this->settings->ntpport, 5);
        if ($socket->connect())
        {
            $ntp = new Client();
            $ntp->setSocket($socket);
            $time = $ntp->getTime();
            $os = $this->operating_system();
            $time->setTimezone(new DateTimeZone(date_default_timezone_get()));
            if ($os == 'linux') {
                exec("sudo date -s '" . $time->format("d M Y H:i:s") . "' 2>&1", $error);
                if (isset($error[1])) {
                    print("error, " . $error[0]);
                    return;
                } else {
                    print("success, time set !");
                    return;
                }
            } elseif ($os == 'win') {
                exec("date -s '" . $time->format("M/d/Y") . "' 2>&1", $error1);
                exec("time -s '" . $time->format("g:i:s A") . "' 2>&1", $error2);
                // more windows stuff to do,time constraints.
            }
        }
        else
        {
            print("error, Unable to connect to the time server !");
        }


    }
    
    public function help() {
        $this->template->title($this->module_details['name'], lang('timespark.help'))->build('admin/help');
    }
    
    public function hours_minutes_seconds($data) {
        $data->hours = range(0, 24);
        unset($data->hours[0]);
        $data->minutes = range(0, 60);
        $data->seconds = range(0, 60);
        return $data;
    }
    
    function operating_system() {
        $os_string = php_uname('s');
        if (strpos(strtoupper($os_string) , 'WIN') !== false) {
            return 'win';
        } else {
            return 'linux';
        }
    }
}
