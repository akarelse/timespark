<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Timespark extends Module {

	public $version = '2.1';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Timespark'
			),
			'description' => array(
				'en' => 'This is a PyroCMS module Timespark.'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
			'menu' => 'content', // You can also place modules in their top level menu. For example try: 'menu' => 'Timespark',
			'sections' => array(
				'items' => array(
					'name' 	=> 'timespark:items', // These are translated from your language file
					'uri' 	=> 'admin/timespark',
						)
				)
		);
	}

	public function install()
	{

		$this->db->delete('settings', array('module' => 'timespark'));

		$ntp_server_setting = array(
			'slug' => 'ntpserver',
			'title' => 'NTP SERVER',
			'description' => 'the server address for the timeserver module',
			'`default`' => 'time.apple.com',
			'`value`' => 'time.apple.com',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'timespark'
		);

		$ntp_server_port = array(
			'slug' => 'ntpport',
			'title' => 'NTP PORT',
			'description' => 'the server port for the timeserver module',
			'`default`' => '123',
			'`value`' => '123',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'timespark'
		);

		if ($this->db->insert('settings', $ntp_server_setting) 
			&& $this->db->insert('settings', $ntp_server_port))
		{
			return TRUE;
		}
	}

	public function uninstall()
	{
		$this->db->delete('settings', array('module' => 'timespark'));

		return TRUE;

	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */
