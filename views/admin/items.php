<section class="title">
	<h4><?php echo lang('timespark:item_list'); ?></h4>
</section>
<section class="item">
	<?php echo form_open('admin/timespark/settime');?>
	
	<fieldset>
	<div ><b id="currentdate"><?php echo lang('timespark:current')?> <?php echo $currenttime ?></b><div>
	<br/>
		<ul>
			<li class="even">
				<label for="title"><?php echo lang('timespark:date');?><span>*</span></label>
				<div class="input"><?php echo form_input('cdate', $cdate, 'maxlength="10" id="datepicker" class="text width-20"'); ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label><?php echo lang('timespark:hours'); ?></label>
				<div class="input"><?php echo form_dropdown('hour', $hours, $chour) ?></div>
			</li>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label><?php echo lang('timespark:minutes'); ?></label>
				<div class="input"><?php echo form_dropdown('minute', $minutes, $cminute) ?></div>
			</li>
		</ul>
	</fieldset>
		<div class="buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )); ?>
	</div>
	
	
	<?php echo form_close(); ?>
</section>